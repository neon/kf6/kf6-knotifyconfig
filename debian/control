Source: kf6-knotifyconfig
Section: libs
Priority: optional
Maintainer: Jonathan Esk-Riddell <jr@jriddell.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               graphviz,
               kf6-extra-cmake-modules,
               kf6-kbookmarks-dev,
               kf6-kcompletion-dev,
               kf6-kconfig-dev,
               kf6-kconfigwidgets-dev,
               kf6-ki18n-dev,
               kf6-kiconthemes-dev,
               kf6-kio-dev,
               kf6-kjobwidgets-dev,
               kf6-knotifications-dev,
               kf6-kservice-dev,
               kf6-kwidgetsaddons-dev,
               kf6-kxmlgui-dev,
               kf6-solid-dev,
               pkg-kde-tools-neon,
               libcanberra-dev,
               qt6-base-dev,
               qt6-tools-dev,
               qt6-phonon-dev
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/frameworks/knotifyconfig
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/knotifyconfig
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/knotifyconfig.git

Package: kf6-knotifyconfig
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: ${misc:Depends}, ${shlibs:Depends}
Replaces: libkf6notifyconfig6,
          libkf6notifyconfig-data,
Description: Configuration system for KNotify.
 This framework offers classes to represent the configuration for an
 event.
 .
 This package contains the data files.

Package: kf6-knotifyconfig-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: kf6-knotifyconfig (= ${binary:Version}),
         qt6-base-dev,
         ${misc:Depends}
Replaces: libkf6notifyconfig-dev,
          libkf6notifyconfig-doc,
Description: development files for knotifyconfig
 This framework offers classes to represent the configuration for an
 event.
 .
 Contains development files for knotifyconfig.

Package: libkf6notifyconfig6
Architecture: all
Depends: kf6-knotifyconfig, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6notifyconfig-data
Architecture: all
Depends: kf6-knotifyconfig, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6notifyconfig-dev
Architecture: all
Depends: kf6-knotifyconfig-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6notifyconfig-doc
Architecture: all
Depends: kf6-knotifyconfig-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
